package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class FlightView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Map<String, String> sort;
    private Map<String, Printable> sortMenu;
    private static Scanner input = new Scanner(System.in);

    public FlightView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - add Airliner");
        menu.put("2", "2 - add Cargo");
        menu.put("3", "3 - print Airplanes database");
        menu.put("4", "4 - print only Airliners");
        menu.put("5", "5 - print only Cargo planes");
        menu.put("6", "6 - sort by filter");
        menu.put("7", "7 - find by fuel usage");
        menu.put("q", "q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);

        sort = new LinkedHashMap<>();
        sort.put("1", "1 - producer");
        sort.put("2", "2 - model");
        sort.put("3", "3 - speed");
        sort.put("4", "4 - range of flight");
        sort.put("5", "5 - fuel usage");
        sort.put("q", "q - to return");

        sortMenu = new LinkedHashMap<>();
        sortMenu.put("1", this::pressButton61);
        sortMenu.put("2", this::pressButton62);
        sortMenu.put("3", this::pressButton63);
        sortMenu.put("4", this::pressButton64);
        sortMenu.put("5", this::pressButton65);
    }

    private void pressButton1() {
        try {
            System.out.println("Enter the next data: producer, model, speed, rangeOfFlight, tank, firstFlight, seats, orders, deliveries, fuelUsage");
            controller.addAirliner(input.nextLine(), input.nextLine(), input.nextInt(), input.nextInt(), input.nextInt(), input.nextLine(), input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt());
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void pressButton2() {
        System.out.println("Enter the next data: producer, model, speed, rangeOfFlight, tank, volume, payload, usage, fuelUsage");
        try {
            controller.addCargo(input.nextLine(), input.nextLine(), input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt(), input.nextLine(), input.nextInt());
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void pressButton3() {
        System.out.println(controller.getAirplaneList());
    }

    private void pressButton4() {
        System.out.println(controller.getAirlinerList());
    }

    private void pressButton5() {
        System.out.println(controller.getCargoList());
    }

    private void pressButton6() {
        showSort();
    }

    private void pressButton61() {
        controller.sortByFilter("1");
    }

    private void pressButton62() {
        controller.sortByFilter("2");
    }

    private void pressButton63() {
        controller.sortByFilter("3");
    }

    private void pressButton64() {
        controller.sortByFilter("4");
    }

    private void pressButton65() {
        controller.sortByFilter("5");
    }

    private void pressButton7(){
        int min;
        int max;

        System.out.println("Please, enter value of minimal and maximum fuel usage: ");
        min = input.nextInt();
        max = input.nextInt();
        System.out.println(controller.getAirplaneList(min, max));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputSortMenu() {
        System.out.println("\nSORT BY:");
        for (String str : sort.values()) {
            System.out.println(str);
        }
    }

    public void showSort() {
        String keyMenu;
        do {
            outputSortMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                sortMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
