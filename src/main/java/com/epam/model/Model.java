package com.epam.model;

import java.util.LinkedList;
import java.util.List;

public class Model {
    List<Airliner> airlinerList;
    List<Cargo> cargoList;
    List<Airplane> airplaneList;

    public Model() {
        airlinerList = new LinkedList<>();
        cargoList = new LinkedList<>();
        airplaneList = new LinkedList<>();
    }

    public List<Airliner> getAirlinerList() {
        return airlinerList;
    }

    public List<Cargo> getCargoList() {
        return cargoList;
    }

    public List<Airplane> getAirplaneList() {
        return airplaneList;
    }

    public void setAirlinerList(List<Airliner> airlinerList) {
        this.airlinerList = airlinerList;
    }

    public void setCargoList(List<Cargo> cargoList) {
        this.cargoList = cargoList;
    }

    public void setAirplaneList(List<Airplane> airplaneList) {
        this.airplaneList = airplaneList;
    }


    public void addCargo(String producer, String model, int speed, int rangeOfFlight, int tank, int volume, int payload, String usage, int fuelUsage) {
        Cargo cargo = new Cargo(producer, model, speed, rangeOfFlight, tank, volume, payload, usage, fuelUsage);
        cargoList.add(cargo);
        airplaneList.add(cargo);
    }

    public void addAirliner(String producer, String model, int speed, int rangeOfFlight, int tank, String firstFlight, int seats, int orders, int deliveries, int fuelUsage) {
        Airliner airliner = new Airliner(producer, model, speed, rangeOfFlight, tank, firstFlight, seats, orders, deliveries, fuelUsage);
        airlinerList.add(airliner);
        airplaneList.add(airliner);
    }
}
