package com.epam.model;

public class Airplane {

    private String producer;
    private String model;
    private int speed;
    private int rangeOfFlight;
    private int tank;
    private int fuelUsage;

    public Airplane(String producer, String model, int speed, int rangeOfFlight, int tank, int fuelUsage) {
        this.producer = producer;
        this.model = model;
        this.speed = speed;
        this.rangeOfFlight = rangeOfFlight;
        this.tank = tank;
        this.fuelUsage = fuelUsage;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }

    public void setRangeOfFlight(int rangeOfFlight) {
        this.rangeOfFlight = rangeOfFlight;
    }

    public int getTank() {
        return tank;
    }

    public void setTank(int tank) {
        this.tank = tank;
    }

    public int getFuelUsage() {
        return fuelUsage;
    }

    public void setFuelUsage(int fuelUsage) {
        this.fuelUsage = fuelUsage;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", speed=" + speed +
                ", rangeOfFlight=" + rangeOfFlight +
                ", tank=" + tank +
                ", fuelUsage=" + fuelUsage +
                '}';
    }
}