package com.epam.model;

public class Airliner extends Airplane {
    private String firstFlight;
    private int seats;
    private int orders;
    private int deliveries;

    public Airliner(String producer, String model, int speed, int rangeOfFlight, int tank, String firstFlight, int seats, int orders, int deliveries, int fuelUsage) {
        super(producer, model, speed, rangeOfFlight, tank, fuelUsage);
        this.firstFlight = firstFlight;
        this.seats = seats;
        this.orders = orders;
        this.deliveries = deliveries;
    }

    public String getFirstFlight() {
        return firstFlight;
    }

    public void setFirstFlight(String firstFlight) {
        this.firstFlight = firstFlight;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public int getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(int deliveries) {
        this.deliveries = deliveries;
    }

    @Override
    public String toString() {
        return "Airliner{" + super.toString() +
                "firstFlight=" + firstFlight +
                ", seats=" + seats +
                ", orders=" + orders +
                ", deliveries=" + deliveries +
                '}';
    }
}
