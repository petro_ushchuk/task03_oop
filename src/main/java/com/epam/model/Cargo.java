package com.epam.model;

public class Cargo extends Airplane {
    private int volume;
    private int payload;
    private String usage;

    public Cargo(String producer, String model, int speed, int rangeOfFlight, int tank, int volume, int payload, String usage, int fuelUsage) {
        super(producer, model, speed, rangeOfFlight, tank, fuelUsage);
        this.volume = volume;
        this.payload = payload;
        this.usage = usage;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "Cargo{" + super.toString() +
                "volume=" + volume +
                ", payload=" + payload +
                ", usage='" + usage + '\'' +
                '}';
    }
}
