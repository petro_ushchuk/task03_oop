package com.epam.controller;

import com.epam.model.Airliner;
import com.epam.model.Airplane;
import com.epam.model.Cargo;

import java.util.List;

public interface Controller {
    List<Airplane> getAirplaneList(int min, int max);
    List<Airplane> getAirplaneList();
    List<Airliner> getAirlinerList();
    List<Cargo> getCargoList();
    void addAirliner(String producer, String model, int speed, int rangeOfFlight, int tank, String firstFlight, int seats, int orders, int deliveries, int fuelUsage);
    void addCargo(String producer, String model, int speed, int rangeOfFlight, int tank, int volume, int payload, String usage, int fuelUsage);
    void sortByFilter(String  filter);
  }
