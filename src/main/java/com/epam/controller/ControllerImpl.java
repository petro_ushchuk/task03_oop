package com.epam.controller;

import com.epam.model.Airliner;
import com.epam.model.Airplane;
import com.epam.model.Cargo;
import com.epam.model.Model;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ControllerImpl implements Controller {

    private Model model = new Model();

    public static final Comparator<Airplane> compareByProducer = (lhs, rhs) -> lhs.getProducer().compareTo(rhs.getProducer());
    public static final Comparator<Airplane> compareByModel = (lhs, rhs) -> lhs.getModel().compareTo(rhs.getModel());
    public static final Comparator<Airplane> compareBySpeed = (lhs, rhs) -> lhs.getSpeed() - (rhs.getSpeed());
    public static final Comparator<Airplane> compareByRangeOfFlight = (lhs, rhs) -> lhs.getRangeOfFlight() - (rhs.getRangeOfFlight());
    public static final Comparator<Airplane> compareByTank = (lhs, rhs) -> lhs.getTank() - (rhs.getTank());
    public static final Comparator<Airplane> compareByFuelUsage = (lhs, rhs) -> lhs.getFuelUsage() - (rhs.getFuelUsage());

    @Override
    public List<Airplane> getAirplaneList() {
        return model.getAirplaneList();
    }

    @Override
    public List<Airliner> getAirlinerList() {
        return model.getAirlinerList();
    }

    @Override
    public List<Cargo> getCargoList() {
        return model.getCargoList();
    }


    @Override
    public void addAirliner(String producer, String m, int speed, int rangeOfFlight, int tank, String firstFlight, int seats, int orders, int deliveries, int fuelUsage) {
        model.addAirliner(producer, m, speed, rangeOfFlight, tank, firstFlight, seats, orders, deliveries, fuelUsage);
    }

    @Override
    public void addCargo(String producer, String m, int speed, int rangeOfFlight, int tank, int volume, int payload, String usage, int fuelUsage) {
        model.addCargo(producer, m, speed, rangeOfFlight, tank, volume, payload, usage, fuelUsage);
    }

    @Override
    public void sortByFilter(String filter) {
        Comparator<Airplane> comparator = compareByProducer;
        if (filter == "1") {
            comparator = compareByProducer;
        }
        if (filter == "2") {
            comparator = compareByModel;
        }
        if (filter == "3") {
            comparator = compareBySpeed;
        }
        if (filter == "4") {
            comparator = compareByRangeOfFlight;
        }
        if (filter == "5") {
            comparator = compareByTank;
        }
        if (filter == "6") {
            comparator = compareByFuelUsage;
        }
        List<Airplane> temp = model.getAirplaneList();
        temp.sort(comparator);
        model.setAirplaneList(temp);
    }

    @Override
    public List<Airplane> getAirplaneList(int min, int max) {
        List<Airplane> allPlanes = model.getAirplaneList();
        List<Airplane> planesWeWant = new LinkedList<>();

        for (Airplane plane : allPlanes) {
            if (plane.getFuelUsage() > min & plane.getFuelUsage() < max)
                planesWeWant.add(plane);
        }
        return planesWeWant;
    }


}
